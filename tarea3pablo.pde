import ddf.minim.*;//libreria para audios
import javax.swing.JOptionPane;//libreria para ventanas de alertas

Minim minim;
AudioPlayer player;
int temp1=799; //temp inicial
int pres1=799; //precion inicial
int velocidad1; //velocidad en la que sube el termometro
int velocidad2; //velocidad en la que sube el medidor de presion
int alerta1=1; //valor logico de la ventana de alerta ( si le dan que si)
int alerta2=1; //valor logico de la ventana de alerta (si le dan que no)
void setup(){
  size (1000,1000);
  background(0);
  noFill();
  
  stroke(0,255,0);
  //termometro 
  beginShape();
  vertex(250,80);
  vertex(350,80);

  vertex(350,800);
  vertex(250,800);
  endShape(CLOSE);
  
  //medidor presion
  
  beginShape();
  
  vertex(800,80);
  vertex(900,80);

  vertex(900,800);
  vertex(800,800);
  endShape(CLOSE);
  
  //texto de los medidores
  PFont letrasans;
  letrasans=loadFont("ComicSansMS-30.vlw");
  textFont(letrasans);
  fill(0,255,0);
  text("Temperatura C°",250,850);
  text("Presion KPa",800,850);
  text("10",200,800);
  text("20",200,750);
   text("30",200,700);
    text("40",200,650);
     text("50",200,600);
      text("60",200,550);
       text("70",200,500);
        text("80",200,450);
         text("90",200,400);
          text("100",200,350);
          text("110",200,300);
          text("120",200,250);
          text("130",200,200);
          text("140",200,150);
          text("150",200,100);
          
 text("10",750,800);
  text("20",750,750);
   text("30",750,700);
    text("40",750,650);
     text("50",750,600);
      text("60",750,550);
       text("70",750,500);
        text("80",750,450);
         text("90",750,400);
          text("100",750,350);
          text("110",750,300);
          text("120",750,250);
          text("130",750,200);
          text("140",750,150);
          text("150",750,100);         
          
   //comando para poder importar sonidos       
  minim=new Minim(this);
  player=minim.loadFile("¡Alerta Nuclear!_50k.mp3");
  
}
//comando para poder ingresar ventanas de alerta
void draw(){
if (temp1==80){player.play(); alerta1 = JOptionPane.showConfirmDialog(null, "Temperatura demasiado alta, reinicie el sistema", "Alerta!", JOptionPane.YES_NO_OPTION);}
if (pres1==80){player.play(); alerta2 = JOptionPane.showConfirmDialog(null, "Presion demasiado alta, reinicie el sistema", "Alerta!", JOptionPane.YES_NO_OPTION);}
 
 paint();
//condiciones para que los termometros suban por el mouse 
 if(alerta1==0){
     temp1=800;
     alerta1=1;
    player.pause();}
    
    
  else if(alerta2==0){
     pres1=800;
     alerta2=1;
   player.pause();}
   

//condiciones para que la temperatura suba
if (abs(mouseX) < 350 &&
      abs(mouseX) > 250 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && temp1>80
      ){
       velocidad1=1;
        temp1 = temp1-velocidad1;
    }   
    
    
else if (abs(mouseX) < 350 &&
      abs(mouseX) > 250 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && temp1==800
      ){
       velocidad1=0;
        temp1 = temp1-velocidad1;
    }      
    
else if (abs(mouseX) < 350 &&
      abs(mouseX) > 250 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && temp1==80
      ){
       velocidad1=0;
        
    }      
 //condiciones para que la temp baje   
else if (abs(mouseX) > 350 && 
        temp1==800){velocidad1=0;}
        
else if (abs(mouseX) < 250 && 
        temp1==800){velocidad1=0;}

else if (abs(mouseY) < 80 && 
        temp1==800){velocidad1=0;}        
        
else if (abs(mouseY) > 800 && 
       temp1==800){velocidad1=0;}             
        
        
        
        
        
else {temp1 = temp1+velocidad1;}


    
//condiciones para que el medidor de presion suba    
if (abs(mouseX) < 900 &&
      abs(mouseX) > 800 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && pres1>80
      ){
       velocidad2=1;
        pres1 = pres1-velocidad2;
    }   
    
    
else if (abs(mouseX) < 900 &&
      abs(mouseX) > 800 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && pres1==800
      ){
       velocidad2=0;
        pres1 = pres1-velocidad2;
    }      
    
else if (abs(mouseX) < 900 &&
      abs(mouseX) > 800 &&
      abs(mouseY) > 80 &&
      abs(mouseY) < 800 && pres1==80
      ){
       velocidad2=0;
        
    }      
//condiciones para que el medidor de presion baje    
else if (abs(mouseX) > 900 && 
        pres1==800){velocidad2=0;}
        
else if (abs(mouseX) < 800 && 
        pres1==800){velocidad2=0;}

else if (abs(mouseY) < 80 && 
        pres1==800){velocidad2=0;}        
        
else if (abs(mouseY) > 800 && 
        pres1==800){velocidad2=0;}             
        
        
        
        
        
else {pres1 = pres1+velocidad2;}





}





//rectangulo rojo que indica de manera visual el llenado de los termometros
void paint(){
  stroke(0,255,0);
  strokeWeight(2.5);
  fill(255);
  quad(250,80,350,80,350,800,250,800);
  
  stroke(0,255,0);
  strokeWeight(2.5);
  fill(255);
  quad(800,80,900,80,900,800,800,800);
  
  
  stroke(255,255,255);
  strokeWeight(0);
  fill(255,0,0);
  quad(250,temp1,350,temp1,350,800,250,800);
  
  stroke(255,255,255);
  strokeWeight(0);
  fill(255,0,0);
  quad(800,pres1,900,pres1,900,800,800,800);
  
  
 
}
